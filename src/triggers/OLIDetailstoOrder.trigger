trigger OLIDetailstoOrder on OrderLineItem__c (before insert, after insert) {

    list<Id> Idlist = new list<Id>();
    set<id> orderidset= new set<id>();
    list<Order__c> listOrdertoUpdate = new list<Order__c>();
    map<id,List<OrderLineItem__c>> orderToOLImap= new map<id,List<OrderLineItem__c>>();
    
    for(OrderLineItem__c oli:trigger.new){
        Idlist.add(oli.Product__c);
        orderidset.add(oli.Order__c);
    }
    
    if(trigger.isBefore){
    
    system.debug('---inbefore');
    Id pricebookId;
        if(!Test.isRunningTest()){
          pricebook2 pb = [select Id from pricebook2 where IsStandard = TRUE][0];
          pricebookId=pb.id;
        }else{
            pricebookId=Test.getStandardPricebookId();
        }
        
        List<pricebookentry> listpbe=[select Product2Id,UnitPrice from pricebookentry where pricebook2Id = :pricebookId and product2Id IN :Idlist];
        
        for(OrderLineItem__c o:trigger.new){
            o.Unit_Price__c=0;
            o.OLI_Total__c=0;
            for(pricebookentry pbe:listpbe){
                If(pbe.Product2Id == o.Product__c){
                    o.Unit_Price__c = pbe.UnitPrice;
                    o.OLI_Total__c = pbe.UnitPrice * o.Quantity__c;
                }
            }
        }
    }
    
    if(trigger.isAfter){
    
    system.debug('---inAfter');
    
      List<OrderLineItem__c> listOLI= [select id,OLI_Total__c,Unit_Price__c, Order__c from OrderLineItem__c where Order__c IN : orderidset];
      
      for(OrderLineItem__c oli : listOLI){
          if(orderToOLImap.get(oli.Order__c)==null){
             orderToOLImap.put(oli.Order__c, new List<OrderLineItem__c>());
          }
          orderToOLImap.get(oli.Order__c).add(oli);
      
      }
      system.debug('===1' +orderToOLImap );
      list<Tax_Calculation__mdt> taxCal = [Select DeveloperName,MasterLabel, Tax_Percent__c from Tax_Calculation__mdt];
      
      for(id orderid : orderToOLImap.keyset()){
          Order__c ord =new Order__c();
          ord.id=orderid;
          ord.Order_Total__c=0;
          
          for(OrderLineItem__c olis : orderToOLImap.get(orderid)){
             ord.Order_Total__c=ord.Order_Total__c + olis.OLI_Total__c;
          }
          
          ord.Tax_Amount__c = ord.Order_Total__c * (taxCal[0].Tax_Percent__c/100);
          ord.Total_Amount__c = ord.Order_Total__c + ord.Tax_Amount__c;
          
          listOrdertoUpdate.add(ord);
          
      }
      
      update listOrdertoUpdate;
         
    }
}