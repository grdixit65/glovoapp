@isTest
private class CustomerInfo_test
{
	 @testSetup
    static void createTestData() { 
        
		List<Product2> prodList= new List<Product2>();
		List<PriceBookEntry> pbeList= new List<PriceBookEntry>();
        Product2 prod = new Product2();
        prod.Name = 'ACC';
        prod.ProductCode = 'TEST';
		prod.Description = 'TEST';
		prod.Product_Price_Unit__c = 1;
		prodList.add(prod);
        
        Product2 prod1 = new Product2();
        prod1.Name = 'BCC';
		prod1.ProductCode = 'TEST1';
		prod1.Description = 'TEST1';
		prod1.Product_Price_Unit__c = 1;
        prodList.add(prod1);

		insert prodList;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe.Product2Id = prodList[0].Id;
        pbe.isActive = true;
        pbe.UnitPrice = 10;
        pbe.PriceBook2Id = Test.getStandardPricebookId();
		pbeList.add(pbe);

		PriceBookEntry pbe1 = new PriceBookEntry();
        pbe1.Product2Id = prodList[1].Id;
        pbe1.isActive = true;
        pbe1.UnitPrice = 10;
        pbe1.PriceBook2Id = Test.getStandardPricebookId();
		pbeList.add(pbe1);
        insert pbeList;
        
		Customer__c custmerinfo=new Customer__c(Name__c= 'test customer',Address__c ='test address',
		                                       Contact_Number__c='7676719650',Email__c='grdixit65@gmail.com',pwd__c ='test');
		insert custmerinfo;

		Order__c order=new Order__c(CustomerInfo__c=custmerinfo.id,Status__c='New',Shipping_Address__c='test Addressa',Billing_Address__c='test address2');
		insert order;
		OrderLineItem__c oli = new OrderLineItem__c(Order__c=order.id,Product__c=prodList[0].Id,Quantity__c=2);
		insert oli;

	}
	static testMethod void testLoginSuccess(){

        Test.StartTest();

			CustomerOrderInfoCtrl testCustmContrl = new CustomerOrderInfoCtrl();
			PageReference pageRef = Page.CustomerOrderInfo;
			pageRef.getParameters().put('input1', 'grdixit65@gmail.com');
			pageRef.getParameters().put('input2', 'test');
			Test.setCurrentPage(pageRef);
			testCustmContrl.redirectafterSignIn(); 
	
        Test.stopTest();
        
        System.assertEquals(testCustmContrl.customerInfo.custEmail, 'grdixit65@gmail.com');
             
    }
	static testMethod void testLoginFailed(){

        Test.StartTest();

			CustomerOrderInfoCtrl testCustmContrl = new CustomerOrderInfoCtrl();
			PageReference pageRef = Page.CustomerOrderInfo;
			pageRef.getParameters().put('input1', 'grdixit65@gmail.com');
			pageRef.getParameters().put('input2', 'test111');
			Test.setCurrentPage(pageRef);
			testCustmContrl.redirectafterSignIn(); 
	
        Test.stopTest();
        
        System.assertEquals(testCustmContrl.LoginerrorMessage, 'User Id and Password not matching');
             
    }
	
}