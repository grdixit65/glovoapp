@RestResource(urlMapping='/custmerInfo/*')
global class GlovoAppAPI{

    @HttpGet
    global static ResponseHandler doGet() {

        system.debug(LoggingLevel.DEBUG,'Entring GlovoAppAPI:');
        ResponseHandler response = new ResponseHandler();
		GlovoAppWrapper.CustomerDetail customerInfo = new GlovoAppWrapper.CustomerDetail ();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Map <String, String> queryParam = req.params;
        String orderNum=queryParam.get('orderNumber');
		String custmId=queryParam.get('customerId');
        
        try {
          if(custmId!=null && custmId!=''){
               GlovoAppService glovoService= new GlovoAppService();
        		customerInfo=glovoService.getcustomerDetails(custmId);
       			glovoService.getOrderDetails(customerInfo);
          }else if(orderNum !=null && orderNum!=''){
			  GlovoAppService glovoService= new GlovoAppService();
       			glovoService.getOrderDetail(customerInfo,orderNum);
		  }
          
          if(customerInfo!=null)
          {
              response.Status = 'Success';
              response.ErrorCode = '';
              response.Data = customerInfo;
              response.Message = 'Success : Found Order';
          }
          
          else
          {
              response.ErrorCode = 'E-0001';
              response.Status = 'error';
              response.Message = 'Fail : Order Not Found';
              
          }
          
          return response;
        
        }catch (Exception e) {
            system.debug(LoggingLevel.ERROR,'Error when Calling GlovoAppAPI :'+e.getMessage());
            
            response.ErrorCode = 'E-0002';
            response.Status = 'error';
            response.Message = 'Internal Serveer Error. try again';
            return response;
        }
    
    }

	  global class ResponseHandler{
        public String Status {get; set;}
        public GlovoAppWrapper.CustomerDetail Data {get;set;}
        public String Message {get;set;}
        public String ErrorCode {get; set;}
      
      }

}
