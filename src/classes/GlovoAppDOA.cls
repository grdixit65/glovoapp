public class GlovoAppDOA {

	 public static Customer__c getCustomerDetail(String custId){
        system.debug(LoggingLevel.DEBUG,'Entering GlovoAppDOA :getCustomerDetail');
        Customer__c custInfo=null;
        try{
        custInfo=[select id,Name__c,Address__c,Contact_Number__c,Email__c,pwd__c from Customer__c where id=:custId];
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while getting values from  DAO getCustomerDetail:'+e.getMessage());
        }
         system.debug(LoggingLevel.DEBUG,'Exiting to  GlovoAppDOA : getCustomerDetail()');
        return custInfo;
    }

	 public static List<Order__c> getOrderDetail(String custId){
        system.debug(LoggingLevel.DEBUG,'Entering GlovoAppDOA :getOrderDetail');
        List<Order__c> orderInfo=New List<Order__c>();
        try{
        orderInfo=[select id,Name,CustomerInfo__c,CreatedDate,Order_Total__c,Tax_Amount__c,Total_Amount__c,Status__c,Shipping_Address__c,Billing_Address__c,Assigned_to_Glover__c,
		(select id,Name,OLI_Total__c,Order__c,Product__c,Quantity__c,Unit_Price__c from OrderLineItems__r) from Order__c where CustomerInfo__c=:custId];
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while getting values from  DAO getOrderDetail:'+e.getMessage());
        }
         system.debug(LoggingLevel.DEBUG,'Exiting to  GlovoAppDOA : getOrderDetail()');
        return orderInfo;
    }

	public static List<Product2> getProductDetail(set<id> prodSet){
        system.debug(LoggingLevel.DEBUG,'Entering GlovoAppDOA :getProductDetail');
        List<Product2>  prodList=New List<Product2> ();
        try{
        prodList=[select id,Product_Price_Unit__c,ProductCode,Name,Description from Product2 where Id IN : prodSet];
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while getting values from  DAO getProductDetail:'+e.getMessage());
        }
         system.debug(LoggingLevel.DEBUG,'Exiting to  GlovoAppDOA : getProductDetail()');
        return prodList;
    }

	 public static Customer__c getLogin(String email){
        system.debug(LoggingLevel.DEBUG,'Entering GlovoAppDOA :getLogin');
        Customer__c custInfo=null;
        try{
        custInfo=[select id,Name__c,Address__c,Contact_Number__c,Email__c,pwd__c from Customer__c where Email__c=:email];
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while getting values from  DAO getLogin:'+e.getMessage());
        }
         system.debug(LoggingLevel.DEBUG,'Exiting to  GlovoAppDOA : getLogin()');
        return custInfo;
    }

	public static List<Order__c> getOrderDetails(String orderNum){
        system.debug(LoggingLevel.DEBUG,'Entering GlovoAppDOA :getOrderDetail');
        List<Order__c> orderInfo=New List<Order__c>();
        try{
        orderInfo=[select id,Name,CustomerInfo__c,CreatedDate,Order_Total__c,Tax_Amount__c,Total_Amount__c,Status__c,Shipping_Address__c,Billing_Address__c,Assigned_to_Glover__c,
		(select id,Name,OLI_Total__c,Order__c,Product__c,Quantity__c,Unit_Price__c from OrderLineItems__r) from Order__c where Name=:orderNum];
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while getting values from  DAO getOrderDetail:'+e.getMessage());
        }
         system.debug(LoggingLevel.DEBUG,'Exiting to  GlovoAppDOA : getOrderDetail()');
        return orderInfo;
    }


}