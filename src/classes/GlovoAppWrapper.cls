public class GlovoAppWrapper {
	public string orderNum{get;set;}

	public class OrderDetail{
        public string orderid{get;set;}
		public string orderNumber{get;set;}
		public string orderShippingAddress{get;set;}
		public string orderBillingAddress{get;set;}
		public double orderTotal{get;set;}
		public double orderTaxAmount{get;set;}
		public double orderNetAmount{get;set;}
		public string orderStatus{get;set;}
		public boolean isActive{get;set;}
		public DateTime orderPlacedon{get;set;}
		public List<OLIDetail> orderOLIs{get;set;}

    }
	public class CustomerDetail{
        public string custId{get;set;}
		public string custName{get;set;}
		public string custEmail{get;set;}
		public string custPwd{get;set;}
		public string custPhone{get;set;}
		public string custAddress{get;set;}
		public List<OrderDetail> custOrder{get;set;}
    }

	public class OLIDetail{
        public string oliId{get;set;}
		public string oliNumber{get;set;}
		public integer quantity{get;set;}
		public string productId{get;set;}
		public string productName{get;set;}
		public string productCode{get;set;}
		public string productDescription{get;set;}
		public decimal productUnitPrice{get;set;}
    }

	public class LoginDetail{
        public string loginId{get;set;}
		public string loginPwd{get;set;}
		public boolean isLoginSuccess{get;set;}
    }
}