public class CustomerOrderInfoCtrl{

     
    public GlovoAppWrapper.LoginDetail loginCred{get;set;}
    public GlovoAppWrapper.CustomerDetail customerInfo{get;set;}
    public string LoginerrorMessage{get;set;}
      

    public CustomerOrderInfoCtrl(){
        loginCred = new GlovoAppWrapper.LoginDetail ();
        customerInfo = new GlovoAppWrapper.CustomerDetail ();

    }

    public PageReference redirectafterSignIn(){
        system.debug(LoggingLevel.DEBUG,'Entering CustomerOrderInfoCtrl:redirectafterSignIn');       
        try{
            loginCred.loginId=Apexpages.currentPage().getParameters().get('input1');
            loginCred.loginPwd=Apexpages.currentPage().getParameters().get('input2');
            GlovoAppService glovoService= new GlovoAppService();
            customerInfo = glovoService.getAuthentication(loginCred);
            if(customerInfo ==null && loginCred.isLoginSuccess !=true){
                LoginerrorMessage = 'User Id and Password not matching';
                ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.Error,'User Id and Password not matching');
                ApexPages.addMessage(msg);
            }else{
                glovoService.getOrderDetails(customerInfo);
            }
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while redirecting to the redirectafterSignIn:'+e.getMessage());
            
            if(e.getMessage().equals('List has no rows for assignment to SObject')){
              ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.Error,'User Id and Password not matching');
                ApexPages.addMessage(msg);
                LoginerrorMessage = 'User Id and Password not matching';
            }
        }
        system.debug(LoggingLevel.DEBUG,'Exiting ProfileInsertDetailController:redirectafterSignIn');
        return null;
     }    

}