@isTest
private class GlovoAppAPI_test
{
	 @testSetup
    static void createTestData() { 
        
		List<Product2> prodList= new List<Product2>();
		List<PriceBookEntry> pbeList= new List<PriceBookEntry>();
        Product2 prod = new Product2();
        prod.Name = 'ACC';
        prod.ProductCode = 'TEST';
		prod.Description = 'TEST';
		prod.Product_Price_Unit__c = 1;
		prodList.add(prod);
        
        Product2 prod1 = new Product2();
        prod1.Name = 'BCC';
		prod1.ProductCode = 'TEST1';
		prod1.Description = 'TEST1';
		prod1.Product_Price_Unit__c = 1;
        prodList.add(prod1);

		insert prodList;
        
        PriceBookEntry pbe = new PriceBookEntry();
        pbe.Product2Id = prodList[0].Id;
        pbe.isActive = true;
        pbe.UnitPrice = 10;
        pbe.PriceBook2Id = Test.getStandardPricebookId();
		pbeList.add(pbe);

		PriceBookEntry pbe1 = new PriceBookEntry();
        pbe1.Product2Id = prodList[1].Id;
        pbe1.isActive = true;
        pbe1.UnitPrice = 10;
        pbe1.PriceBook2Id = Test.getStandardPricebookId();
		pbeList.add(pbe1);
        insert pbeList;
        
		Customer__c custmerinfo=new Customer__c(Name__c= 'test customer',Address__c ='test address',
		                                       Contact_Number__c='7676719650',Email__c='grdixit65@gmail.com',pwd__c ='test');
		insert custmerinfo;

		Order__c order=new Order__c(CustomerInfo__c=custmerinfo.id,Status__c='New',Shipping_Address__c='test Addressa',Billing_Address__c='test address2');
		insert order;
		OrderLineItem__c oli = new OrderLineItem__c(Order__c=order.id,Product__c=prodList[0].Id,Quantity__c=2);
		insert oli;

	}
	static testMethod void testAppAPICustmId(){

        Test.StartTest();

		GlovoAppAPI appAPI= new GlovoAppAPI();
		GlovoAppAPI.ResponseHandler reponseVal= new GlovoAppAPI.ResponseHandler();
		
		Customer__c custmerinfo=[select id from Customer__c limit 1];

		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/custmerInfo'; 
        req.params.put('customerId', custmerinfo.id);
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;

		reponseVal = GlovoAppAPI.doGet();
	
        Test.stopTest();

             
    }

	static testMethod void testAppAPIOrderNum(){

        Test.StartTest();

		GlovoAppAPI appAPI= new GlovoAppAPI();
		GlovoAppAPI.ResponseHandler reponseVal= new GlovoAppAPI.ResponseHandler();
		
		Order__c order=[select id,Name from Order__c limit 1];

		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/custmerInfo'; 
        req.params.put('orderNumber', order.Name);
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;

		reponseVal = GlovoAppAPI.doGet();
			
        Test.stopTest();
             
    }

	static testMethod void testAppAPIOrderNumFail(){

        Test.StartTest();

		GlovoAppAPI appAPI= new GlovoAppAPI();
		GlovoAppAPI.ResponseHandler reponseVal= new GlovoAppAPI.ResponseHandler();

		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/custmerInfo'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;

		reponseVal = GlovoAppAPI.doGet();
		
        Test.stopTest();
       
    }

}