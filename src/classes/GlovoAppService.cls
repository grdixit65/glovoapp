public class GlovoAppService {

	public GlovoAppWrapper.CustomerDetail getAuthentication(GlovoAppWrapper.LoginDetail loginDetail){
       Customer__c custmerinfo=null;
	   GlovoAppWrapper.CustomerDetail custmerDetils=null;
        try{
            if(loginDetail!=null){
                custmerinfo= GlovoAppDOA.getLogin(loginDetail.loginId);
                
                if(custmerinfo!=null && loginDetail.loginPwd==custmerinfo.pwd__c){
					custmerDetils=new GlovoAppWrapper.CustomerDetail();
                   custmerDetils.custName=custmerinfo.Name__c;
				   custmerDetils.custAddress=custmerinfo.Address__c;
				   custmerDetils.custPhone=string.valueOf(custmerinfo.Contact_Number__c);
				   custmerDetils.custEmail=custmerinfo.Email__c;
				   custmerDetils.custId=custmerinfo.Id;
				   loginDetail.isLoginSuccess =true;
                }    
            }
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while Authenticating:'+e.getMessage());
        }
        return custmerDetils;
    }


	public List<GlovoAppWrapper.OrderDetail> getOrderDetails(GlovoAppWrapper.CustomerDetail custmerDetils){

	   //List<GlovoAppWrapper.OrderDetail> orderList=null;
	   List<Order__c> ordersLst=null;
	   Set<id> productIdSet = new Set<Id>();
	   List<Product2> prodList=null;

        try{
            if(custmerDetils!=null){
				custmerDetils.custOrder = new List<GlovoAppWrapper.OrderDetail> ();
                ordersLst= GlovoAppDOA.getOrderDetail(custmerDetils.custId);
				prodList = new List<Product2> ();

                if(ordersLst!=null){
					for(Order__c ord: ordersLst){
						GlovoAppWrapper.OrderDetail orderObj=new GlovoAppWrapper.OrderDetail();
                        orderObj.orderid=ord.Id;
						orderObj.orderNumber=ord.Name;
						orderObj.orderShippingAddress=ord.Shipping_Address__c;
						orderObj.orderBillingAddress=ord.Billing_Address__c;
						orderObj.orderTotal=ord.Order_Total__c;
						orderObj.orderTaxAmount=ord.Tax_Amount__c;
						orderObj.orderNetAmount=ord.Total_Amount__c;
						orderObj.orderStatus=ord.Status__c;
						orderObj.orderPlacedon=ord.CreatedDate;
						if(orderObj.orderStatus == 'New' || orderObj.orderStatus == 'In Transit' ||
						orderObj.orderStatus == 'Dispatched'){
							orderObj.isActive=true;
						}else{
							orderObj.isActive=false;
						}
						orderObj.orderOLIs = new List<GlovoAppWrapper.OLIDetail>();

						for(OrderLineItem__c oli : ord.OrderLineItems__r){
                              GlovoAppWrapper.OLIDetail olidetail= new GlovoAppWrapper.OLIDetail();
							  olidetail.oliId= oli.Id;
							  olidetail.oliNumber= oli.Name;
							  olidetail.quantity= integer.valueof(oli.Quantity__c);
							  olidetail.productId= oli.Product__c;
							  productIdSet.add(oli.Product__c);
							  orderObj.orderOLIs.add(olidetail);
						}
						custmerDetils.custOrder.add(orderObj);

					}

					prodList = GlovoAppDOA.getProductDetail(productIdSet);
					Map<id,Product2> prodMap = new 	Map<id,Product2> ();
					for(Product2 prod : prodList){
						prodMap.put(prod.Id, Prod);
					}

					for(GlovoAppWrapper.OrderDetail ord: custmerDetils.custOrder){
						for(GlovoAppWrapper.OLIDetail oli : ord.orderOLIs){
                            oli.productName = prodMap.get(oli.productId).Name;
							oli.productCode = prodMap.get(oli.productId).ProductCode;
							oli.productDescription = prodMap.get(oli.productId).Description;
							oli.productUnitPrice = prodMap.get(oli.productId).Product_Price_Unit__c;
						}
					}
                }    
            }
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while getting orders:'+e.getMessage());
        }
        return custmerDetils.custOrder;
    }

	public GlovoAppWrapper.CustomerDetail getcustomerDetails(string custid){
       Customer__c custmerinfo=null;
	   GlovoAppWrapper.CustomerDetail custmerDetils=null;
        try{
            if(custid!=null){
                custmerinfo= GlovoAppDOA.getCustomerDetail(custid);
                
                if(custmerinfo!=null){
					custmerDetils=new GlovoAppWrapper.CustomerDetail();
                   custmerDetils.custName=custmerinfo.Name__c;
				   custmerDetils.custAddress=custmerinfo.Address__c;
				   custmerDetils.custPhone=string.valueOf(custmerinfo.Contact_Number__c);
				   custmerDetils.custEmail=custmerinfo.Email__c;
				   custmerDetils.custId=custmerinfo.Id;
                }    
            }
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while Authenticating:'+e.getMessage());
        }
        return custmerDetils;
    }

	public void getOrderDetail(GlovoAppWrapper.CustomerDetail custmerDetils, string orderNum){

	   //List<GlovoAppWrapper.OrderDetail> orderList=null;
	   List<Order__c> ordersLst=null;
	   Set<id> productIdSet = new Set<Id>();
	   List<Product2> prodList=null; 

        try{
            if(custmerDetils!=null){
				custmerDetils.custOrder = new List<GlovoAppWrapper.OrderDetail> ();
                ordersLst= GlovoAppDOA.getOrderDetails(orderNum);
				prodList = new List<Product2> ();

                if(ordersLst!=null){
					for(Order__c ord: ordersLst){
						GlovoAppWrapper.OrderDetail orderObj=new GlovoAppWrapper.OrderDetail();
                        orderObj.orderid=ord.Id;
						orderObj.orderNumber=ord.Name;
						orderObj.orderShippingAddress=ord.Shipping_Address__c;
						orderObj.orderBillingAddress=ord.Billing_Address__c;
						orderObj.orderTotal=ord.Order_Total__c;
						orderObj.orderTaxAmount=ord.Tax_Amount__c;
						orderObj.orderNetAmount=ord.Total_Amount__c;
						orderObj.orderStatus=ord.Status__c;
						orderObj.orderPlacedon=ord.CreatedDate;
						if(orderObj.orderStatus == 'New' || orderObj.orderStatus == 'In Transit' ||
						orderObj.orderStatus == 'Dispatched'){
							orderObj.isActive=true;
						}else{
							orderObj.isActive=false;
						}
						orderObj.orderOLIs = new List<GlovoAppWrapper.OLIDetail>();

						for(OrderLineItem__c oli : ord.OrderLineItems__r){
                              GlovoAppWrapper.OLIDetail olidetail= new GlovoAppWrapper.OLIDetail();
							  olidetail.oliId= oli.Id;
							  olidetail.oliNumber= oli.Name;
							  olidetail.quantity= integer.valueof(oli.Quantity__c);
							  olidetail.productId= oli.Product__c;
							  productIdSet.add(oli.Product__c);
							  orderObj.orderOLIs.add(olidetail);
						}
						custmerDetils.custOrder.add(orderObj);

					}

					prodList = GlovoAppDOA.getProductDetail(productIdSet);
					Map<id,Product2> prodMap = new 	Map<id,Product2> ();
					for(Product2 prod : prodList){
						prodMap.put(prod.Id, Prod);
					}

					for(GlovoAppWrapper.OrderDetail ord: custmerDetils.custOrder){
						for(GlovoAppWrapper.OLIDetail oli : ord.orderOLIs){
                            oli.productName = prodMap.get(oli.productId).Name;
							oli.productCode = prodMap.get(oli.productId).ProductCode;
							oli.productDescription = prodMap.get(oli.productId).Description;
							oli.productUnitPrice = prodMap.get(oli.productId).Product_Price_Unit__c;
						}
					}
                }    
            }
        }catch(Exception e){
            system.debug(LoggingLevel.ERROR,'Error while getting orders:'+e.getMessage());
        }
       
    }
    
	
}